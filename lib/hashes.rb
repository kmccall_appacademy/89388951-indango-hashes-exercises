# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_hash = {}
  str.split.each { |word| word_hash[word] = word.length }
  word_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |k, v| older[k] = v }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_count_hash = Hash.new(0)
  word.chars.each { |char| letter_count_hash[char] += 1 }
  letter_count_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  arr_hash = Hash.new(0)
  arr.each { |el| arr_hash[el] += 1 }
  arr_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  num_hash = { even: 0, odd: 0 }
  numbers.each do |num|
    num_hash[:even] += 1 if num.even?
    num_hash[:odd] += 1 if num.odd?
  end
  num_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  str_hash = Hash.new(0)
  string.delete(" ").chars.each { |char| str_hash[char] += 1 if "aeiou".include?(char)}
  str_hash.sort_by { |k, v| v }[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  student_names = []
  students.each { |k, v| student_names << k if v > 6}

  combinations_arr = []
  student_names.each_index do |idx|
    ((idx + 1)...student_names.length).each do |el|
      combinations_arr << [student_names[idx], student_names[el]]
    end
  end
  # my original way
  # sub_arr = []
  # student_names.each_with_index do |name, idx|
  #   sub_arr << [name, student_names[(idx+1) % student_names.length]]
  # end
  combinations_arr
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  biodiversity_idx = Hash.new(0)
  specimens.each { |animal| biodiversity_idx[animal] += 1 }

  num_of_species = biodiversity_idx.length
  smallest_pop = biodiversity_idx.sort_by { |k, v| v }[0][1]
  largest_pop = biodiversity_idx.sort_by { |k, v| v }[-1][1]

  (num_of_species ** 2) * (smallest_pop/largest_pop)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_hash = Hash.new(0)
  normal_sign.delete(" ").downcase.chars.each { |el| normal_hash[el] += 1 }

  vandalized_hash = Hash.new(0)
  vandalized_sign.delete(" ").downcase.chars.each { |el| vandalized_hash[el] += 1 }

  vandalized_hash.each { |k, v| return false if normal_hash[k] < v || normal_hash[k] == false }

  true
end

def character_count(str)
end
